<?php

declare(strict_types=1);
namespace Drupal\user_secure_tokens;

final class SecureTokens {

  protected ?\WeakReference $enablerWeakReference = NULL;

  public function __construct() {}

  public static function getService(): self {
    return \Drupal::service('user_secure_tokens.service');
  }

  public function acquireEnabler(): SecureTokensEnabler {
    if (
      $this->enablerWeakReference
      && ($enabler = $this->enablerWeakReference->get())
    ) {
      return $enabler;
    }
    else {
      $enabler = new SecureTokensEnabler();
      $this->enablerWeakReference = \WeakReference::create($enabler);
      return $enabler;
    }
  }

  public function hasEnabler() {
    return $this->enablerWeakReference
      && boolval($this->enablerWeakReference->get());
  }

}
