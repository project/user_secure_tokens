<?php

declare(strict_types=1);
namespace Drupal\Tests\user_secure_tokens\Unit;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\UserInterface;
use Drupal\user_secure_tokens\SecureTokens;

/**
 * @group user_secure_tokens
 */
final class UserSecureTokensTest extends KernelTestBase {

  use UserCreationTrait;

  protected UserInterface $user;

  protected static $modules = [
    'system',
    'user',
    'user_secure_tokens',
  ];

  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');

    $this->user = $this->createUser();
  }

  public function provideTestTokens(): array {
    return [
      ['[user:one-time-login-url]'],
      ['[user:cancel-url]'],
    ];
  }

  /**
   * Secure tokens need enabler.
   *
   * @dataProvider provideTestTokens
   */
  public function testTokens(string $tokenText) {
    $this->assertEmpty($this->replaceTokenText($tokenText));

    $enable = SecureTokens::getService()->acquireEnabler();
    $this->assertNotEmpty($this->replaceTokenText($tokenText));
    unset($enable);

    $this->assertEmpty($this->replaceTokenText($tokenText));
  }

  protected function replaceTokenText(string $text): string {
    $data = [
      'user' => $this->user,
    ];
    return \Drupal::token()->replacePlain($text, $data, ['clear' => TRUE]);
  }

}
