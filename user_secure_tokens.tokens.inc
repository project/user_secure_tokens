<?php

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\user_secure_tokens\SecureTokens;

/**
 * Implements hook_token_info().
 */
function user_secure_tokens_token_info() {
  $tokens = [];
  if (SecureTokens::getService()->hasEnabler()) {
    $tokens['one-time-login-url'] = [
      'name' => t('One-time login URL for user'),
      'description' => t('Clicking this URL will login the user without further checks.'),
    ];
    $tokens['cancel-url'] = [
      'name' => t('Cancel URL for user'),
      'description' => t('Clicking this URL will cancel the user without further checks.'),
    ];
  }
  return [
    'tokens' => [
      'user' => $tokens,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function user_secure_tokens_tokens($type, $tokens, $data, $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if (SecureTokens::getService()->hasEnabler()) {
    if ($type === 'user') {
      $user = $data['user'] ?? NULL;
      if ($user  instanceof \Drupal\user\UserInterface) {
        foreach ($tokens as $key => $token) {
          switch ($key) {
            case 'one-time-login-url':
              $replacements[$token] = user_pass_reset_url($user, $options);
              break;
            case 'cancel-url':
              $replacements[$token] = user_cancel_url($user, $options);
              break;
          }
        }
      }
    }
  }
  return $replacements;
}
